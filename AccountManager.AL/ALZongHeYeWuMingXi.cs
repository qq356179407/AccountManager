﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.TableModel.Model;
using AccountManager.AL.Enums;
using ZhCun.Framework.Common.Models;

namespace AccountManager.AL
{
    public class ALZongHeYeWuMingXi : ALBase
    {
        public class ConsumeSearchModel
        {
            public DateTime StartTime { set; get; }
            public DateTime EndTime { set; get; }
            public string ConsumeName { set; get; }
            public string ConsumeType { set; get; }
            public int BalanceType { set; get; }
            public string Account { set; get; }
            public bool IsAddTime { set; get; }
            public int ChongZhangBiaoZhi { set; get; }
        }
        public List<VConsume> GetConsumeData(ConsumeSearchModel serModel)
        {
            CommonSearch.Clear();
            if (serModel.IsAddTime)
            {
                CommonSearch
                    .And(VConsume.CNAddTime).GreaterThenEqual(serModel.StartTime.ToString("yyyy-MM-dd"))
                    .And(VConsume.CNAddTime).LessThan(serModel.EndTime.AddDays(1).ToString("yyyy-MM-dd"));
                CommonSearch.OrderByDesc(VConsume.CNAddTime);
            }
            else
            {
                CommonSearch
                    .And(VConsume.CNConsumeDate).GreaterThenEqual(serModel.StartTime.ToString("yyyy-MM-dd"))
                    .And(VConsume.CNConsumeDate).LessThan(serModel.EndTime.AddDays(1).ToString("yyyy-MM-dd"));
                CommonSearch.OrderByDesc(VConsume.CNConsumeDate);
            }
            if (!string.IsNullOrEmpty(serModel.ConsumeName))
            {
                CommonSearch.And(VConsume.CNConsumeName).LikeFull(serModel.ConsumeName);
            }
            if (!string.IsNullOrEmpty(serModel.ConsumeType))
            {                
                CommonSearch.And(VConsume.CNConsumeTypeId).Equal(serModel.ConsumeType);
            }
            if (serModel.BalanceType != 0)
            {
                CommonSearch.And(VConsume.CNBalanceTypeId).Equal(serModel.BalanceType);
            }
            if (!string.IsNullOrEmpty(serModel.Account))
            {
                CommonSearch.And(VConsume.CNAccountId).Equal(serModel.Account);
            }
            if (serModel.ChongZhangBiaoZhi != 0)
            {
                CommonSearch.And(VConsume.CNIsCancel).Equal(serModel.ChongZhangBiaoZhi == 1 ? true : false);
            }           
            List<VConsume> rList = DA.GetList<VConsume>(CommonSearch);            
            return rList;
        }

        public List<VConsume> GetConsumeData(ISearch adSearchObj)
        {
            var  rList = DA.GetList<VConsume>(adSearchObj);
            return rList;
        }
    }
}