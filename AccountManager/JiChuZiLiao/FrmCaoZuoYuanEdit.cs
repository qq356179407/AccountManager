﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.TableModel.Model;
using AccountManager.AL.Enums;
using AccountManager.AL;
using ZhCun.Framework.Common.Helpers;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmCaoZuoYuanEdit : BaseForm
    {
        public FrmCaoZuoYuanEdit(EditFormHandle<TOperator> EdidHandle)
            : this(EdidHandle,null)
        {

        }
        public FrmCaoZuoYuanEdit(EditFormHandle<TOperator> EdidHandle,TOperator model)
        {
            InitializeComponent();
            _Model = model;
            this.EdidHandle = EdidHandle;
        }

        TOperator _Model;
        EditFormHandle<TOperator> EdidHandle;

        private void FrmCaoZuoYuanEdit_Load(object sender, EventArgs e)
        {            
            if (_Model != null)
            {
                dcm1.SetControlValue(_Model);
            }
            else
            {
                _Model = new TOperator();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            dcm1.SetValueToClassObj(_Model);
            string errMsg;
            bool r = EdidHandle(_Model, out errMsg);
            if (!r)
            {
                ShowMessage(errMsg);
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void txtName_Leave(object sender, EventArgs e)
        {
            string py = StringHelper.GetPinYin(txtName.Text,true);
            txtNamePY.Text = py;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}