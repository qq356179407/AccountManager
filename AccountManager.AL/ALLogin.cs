﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.TableModel.Model;
using AccountManager.Runtime;

namespace AccountManager.AL
{
    public class ALLogin : ALBase
    {
        public bool Login(string loginName, string loginPwd)
        {
            CommonSearch.Clear();
            CommonSearch.Add(TOperator.CNLoginName).Equal(loginName).And(TOperator.CNLoginPwd).Equal(loginPwd);
            var rList = DA.GetList<TOperator>(CommonSearch);
            if (rList != null && rList.Count == 1)
            {
                TOperator model = rList[0];
                LoginInfoModel loginInfoModel = new LoginInfoModel();
                loginInfoModel.LoginName = model.LoginName;
                loginInfoModel.LoginTime = GetNowTime();
                loginInfoModel.Name = model.Name;
                loginInfoModel.OperatorId = model.Id;
                LoginInfo.SetLoginInfo(loginInfoModel);
                return true;
            }
            return false;
        }

        public bool UpdateUserInfo(string newUserName, string newLoginName, out string errMsg)
        {
            if (string.IsNullOrEmpty(newUserName) || string.IsNullOrEmpty(newLoginName))
            {
                errMsg = "登录名或用户名不能为空!";
                return false;
            }
            CommonSearch.Clear();
            CommonSearch
                .Add(TOperator.CNLoginName).Equal(newLoginName)
                .Or(TOperator.CNName).Equal(newUserName)
                .AddGroup()
                .And(TOperator.CNId).EqualNot(LoginInfo.OperatorId);
            bool r = DA.IsExist<TOperator>(CommonSearch);
            if (r)
            {
                errMsg = "用户名或登录名已被占用!";
                return false;
            }
            TOperator model = new TOperator();
            model.Id = LoginInfo.OperatorId;
            model.LastTime = GetNowTime();
            model.Name = newUserName;
            model.LoginName = newLoginName;
            DA.Update<TOperator>(model, TOperator.CNLoginName, TOperator.CNName, TOperator.CNLastTime);
            errMsg = null;

            LoginInfo.LoginState.LoginName = newLoginName;
            LoginInfo.LoginState.Name = newUserName;

            return true;
        }

        public bool ChangePassword(string oldPwd, string newPwd, string rNewPwd,out string errMsg)
        {
            if (newPwd != rNewPwd)
            {
                errMsg = "两次密码不一致!";
                return false;
            }
            CommonSearch.Clear();
            CommonSearch.Add(TOperator.CNId).Equal(LoginInfo.OperatorId).And(TOperator.CNLoginPwd).Equal(oldPwd);
            bool r = DA.IsExist<TOperator>(CommonSearch);
            if (!r)
            {
                errMsg = "原密码不正确!";
                return false;
            }
            TOperator model = new TOperator();
            model.Id = LoginInfo.OperatorId;
            model.LastTime = GetNowTime();
            model.LoginPwd = newPwd;
            DA.Update(model, TOperator.CNLastTime, TOperator.CNLoginPwd);
            errMsg = null;
            return true;
        }
    }
}
