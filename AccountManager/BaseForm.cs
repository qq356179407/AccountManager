﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZhCun.Framework.WinCommon.Forms;

namespace AccountManager
{
    public partial class BaseForm : FrmBase
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        #region 显示LabelMessage的方法

        /// <summary>
        /// 显示LabelMessage提示消息
        /// </summary>
        protected void ShowMessage(string msg, params object[] args)
        {
            string msgText = string.Empty;
            if (args == null || args.Length == 0)
                msgText = msg;
            else msgText = string.Format(msg, args);
            lbMessage.ShowMessage(msgText);
        }
        /// <summary>
        /// 显示LabelMessage指定显示时间(单位:秒)
        /// </summary>
        public void ShowMessage(string msgText, int time)
        {
            lbMessage.ShowMessage(msgText, time);
        }
        /// <summary>
        /// 显示LabelMessage指定显示时间(单位秒)及显示一定时间后的处理事件
        /// </summary>
        public void ShowMessage(string msgText, int showSec, EventHandler msgAfterEvent)
        {
            lbMessage.ShowMessage(msgText, showSec, msgAfterEvent);
        }

        #endregion

        #region 常用询问框

        /// <summary>
        /// 询问提示框,点击OK 返回true
        /// </summary>
        public bool ShowQuestion(string msg, string caption, params object[] msgArgs)
        {
            string msgText = string.Format(msg, msgArgs);
            DialogResult r = MessageBox.Show(msgText, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button2);
            return r == System.Windows.Forms.DialogResult.OK ? true : false;
        }

        #endregion
    }
}
