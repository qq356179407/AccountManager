﻿namespace AccountManager.ChaXunTongJi
{
    partial class FrmLeiXingRiTongJi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbRecordCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbSumAmount = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgv = new ZhCun.Framework.WinCommon.Controls.ZCDataGridView();
            this.dcm1 = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.chBoxIsAddTime = new System.Windows.Forms.CheckBox();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.chBoxOnlyHaveAmount = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tslbRecordCount,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.tslbSumAmount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 573);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(986, 22);
            this.statusStrip1.TabIndex = 29;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(56, 17);
            this.toolStripStatusLabel1.Text = "记录数：";
            // 
            // tslbRecordCount
            // 
            this.tslbRecordCount.BackColor = System.Drawing.Color.Transparent;
            this.tslbRecordCount.ForeColor = System.Drawing.Color.Red;
            this.tslbRecordCount.Name = "tslbRecordCount";
            this.tslbRecordCount.Size = new System.Drawing.Size(20, 17);
            this.tslbRecordCount.Text = "０";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(11, 17);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(80, 17);
            this.toolStripStatusLabel3.Text = "账户增加额：";
            // 
            // tslbSumAmount
            // 
            this.tslbSumAmount.BackColor = System.Drawing.Color.Transparent;
            this.tslbSumAmount.ForeColor = System.Drawing.Color.Red;
            this.tslbSumAmount.Name = "tslbSumAmount";
            this.tslbSumAmount.Size = new System.Drawing.Size(20, 17);
            this.tslbSumAmount.Text = "０";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.DisplayRowCount = true;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 76);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(986, 519);
            this.dgv.TabIndex = 30;
            this.dgv.UseControlStyle = true;
            // 
            // chBoxIsAddTime
            // 
            this.chBoxIsAddTime.Checked = true;
            this.chBoxIsAddTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dcm1.SetIsUse(this.chBoxIsAddTime, true);
            this.chBoxIsAddTime.Location = new System.Drawing.Point(453, 30);
            this.chBoxIsAddTime.Name = "chBoxIsAddTime";
            this.chBoxIsAddTime.Size = new System.Drawing.Size(123, 21);
            this.chBoxIsAddTime.TabIndex = 19;
            this.dcm1.SetTagColumnName(this.chBoxIsAddTime, null);
            this.chBoxIsAddTime.Text = "记账时间";
            this.dcm1.SetTextColumnName(this.chBoxIsAddTime, "IsAddTime");
            this.chBoxIsAddTime.UseVisualStyleBackColor = true;
            // 
            // dtpStartDate
            // 
            this.dcm1.SetIsUse(this.dtpStartDate, true);
            this.dtpStartDate.Location = new System.Drawing.Point(82, 29);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(133, 21);
            this.dtpStartDate.TabIndex = 17;
            this.dcm1.SetTagColumnName(this.dtpStartDate, null);
            this.dcm1.SetTextColumnName(this.dtpStartDate, "StartDate");
            // 
            // dtpEndDate
            // 
            this.dcm1.SetIsUse(this.dtpEndDate, true);
            this.dtpEndDate.Location = new System.Drawing.Point(299, 29);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(133, 21);
            this.dtpEndDate.TabIndex = 18;
            this.dcm1.SetTagColumnName(this.dtpEndDate, null);
            this.dcm1.SetTextColumnName(this.dtpEndDate, "EndDate");
            // 
            // chBoxOnlyHaveAmount
            // 
            this.chBoxOnlyHaveAmount.AutoSize = true;
            this.dcm1.SetIsUse(this.chBoxOnlyHaveAmount, true);
            this.chBoxOnlyHaveAmount.Location = new System.Drawing.Point(538, 32);
            this.chBoxOnlyHaveAmount.Name = "chBoxOnlyHaveAmount";
            this.chBoxOnlyHaveAmount.Size = new System.Drawing.Size(132, 16);
            this.chBoxOnlyHaveAmount.TabIndex = 25;
            this.dcm1.SetTagColumnName(this.chBoxOnlyHaveAmount, null);
            this.chBoxOnlyHaveAmount.Text = "是否隐藏无数据类型";
            this.dcm1.SetTextColumnName(this.chBoxOnlyHaveAmount, "OnlyHaveAmount");
            this.chBoxOnlyHaveAmount.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chBoxOnlyHaveAmount);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.chBoxIsAddTime);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpStartDate);
            this.groupBox1.Controls.Add(this.dtpEndDate);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(986, 76);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(759, 24);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 30);
            this.btnSearch.TabIndex = 24;
            this.btnSearch.Text = "查询";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "开始日期:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(235, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 16;
            this.label4.Text = "结束日期:";
            // 
            // FrmLeiXingRiTongJi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 595);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmLeiXingRiTongJi";
            this.Text = "账目类型统计(日报表)";
            this.Load += new System.EventHandler(this.FrmLeiXingRiTongJi_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.dgv, 0);
            this.Controls.SetChildIndex(this.statusStrip1, 0);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tslbRecordCount;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel tslbSumAmount;
        private ZhCun.Framework.WinCommon.Components.DCM dcm1;
        private ZhCun.Framework.WinCommon.Controls.ZCDataGridView dgv;
        private System.Windows.Forms.CheckBox chBoxIsAddTime;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chBoxOnlyHaveAmount;

    }
}