﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using AccountManager.TableModel.Model;

namespace AccountManager
{
    public partial class FrmZhuangZhang : BaseForm
    {
        public FrmZhuangZhang()
            : this(null)
        {
        }
        public FrmZhuangZhang(TAccount acCount)
        {
            InitializeComponent();
            this._AccountMode = acCount;
        }

        TAccount _AccountMode;
        ALZhuangZhang _ALObj;
        ALZhuangZhang.ZhuanZhangModel _Model;

        private void FrmZhuangZhang_Load(object sender, EventArgs e)
        {
            _ALObj = new ALZhuangZhang();
            _Model = new ALZhuangZhang.ZhuanZhangModel();
            ComboxListHelper.SetAccount(comboxList1, txtAccountIn);
            ComboxListHelper.SetAccount(comboxList1, txtAccountOut);
            ComboxListHelper.SetConsumeType(comboxList1, txtConsumeType, AL.Enums.EnConsumeTypeCategory.GongGong);
            if (_AccountMode != null)
            {
                txtAccountOut.Text = _AccountMode.ActName;
                txtAccountOut.Tag = _AccountMode.Id;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            dcm1.SetValueToClassObj(_Model);
            string errMsg;
            if (!_ALObj.ZhuanZhang(_Model, out errMsg))
            {
                ShowMessage(errMsg);
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
