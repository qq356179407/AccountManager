﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;

namespace AccountManager.ChaXunTongJi
{
    public partial class FrmZhangHuRiTongJi : BaseForm
    {
        public FrmZhangHuRiTongJi()
        {
            InitializeComponent();
        }
        ALZhangHuRiTongJi _ALObj;
        ALZhangHuRiTongJi.SearchModel _SearchModel;
        private void FrmZhangHuRiTongJi_Load(object sender, EventArgs e)
        {
            _ALObj = new ALZhangHuRiTongJi();
            dgv.AutoGenerateColumns = true;
            _SearchModel = new ALZhangHuRiTongJi.SearchModel();
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            decimal sumAmount;
            dcm1.SetValueToClassObj(_SearchModel);
            DataTable dt = _ALObj.GetData(_SearchModel,out sumAmount);
            dgv.DataSource = dt;
            tslbRecordCount.Text = dt.Rows.Count.ToString();
            tslbSumAmount.Text = sumAmount.ToString();
        }
    }
}