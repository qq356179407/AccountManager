﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using System.Reflection;
using ZhCun.Framework.Common;

namespace AccountManager
{
    public partial class FrmLogin : BaseForm
    {
        public FrmLogin()
            : this(string.Empty)
        { }
        /// <summary>
        /// 当用户名不为空时,则锁定用户名,不允许输入
        /// </summary>
        public FrmLogin(string loginName)
        {
            InitializeComponent();
            _DefaultLoginName = loginName;
        }

        ALLogin _ALObj;
        int _ErrCount;
        string _DefaultLoginName;
        public string FormCaption
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            _ALObj = new ALLogin();
            _ErrCount = 0;
            if (!string.IsNullOrEmpty(_DefaultLoginName))
            {
                txtLoginName.Text = _DefaultLoginName;
                txtLoginName.Enabled = false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string loginName = txtLoginName.Text;
            string loginPwd = txtPwd.Text;
            bool r = _ALObj.Login(loginName, loginPwd);
            if (r)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                MessageBox.Show("用户名或密码错误!");
                _ErrCount++;
            }
            if (_ErrCount > 3)
            {
                MessageBox.Show("输错密码超过3次,程序即将退出!");
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtLoginName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPwd.Focus();
            }
        }

        private void txtPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnOK_Click(sender, e);
            }
        }
    }
}