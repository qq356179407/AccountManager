﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using ZhCun.Framework.Common;
using System.IO;

namespace AccountManager
{
    public partial class FrmDataManager : BaseForm
    {
        public FrmDataManager()
        {
            InitializeComponent();
        }

        ALDataManager _ALObj;
        bool _IsRestoreData = false;
        public bool IsRestoreData
        {
            get
            {
                return _IsRestoreData;
            }
        }

        public string GetDBFilePath()
        {
            string connStr = Configurations.GetConnectString("Main");
            //Data Source=D:\ZhangCun\AccountManager.db
            string fileName = connStr.Replace("Data Source=", "");
            return fileName;
        }

        private void FrmDataManager_Load(object sender, EventArgs e)
        {
            _ALObj = new ALDataManager();
        }

        private void btnDataBak_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "数据备份文件|*.bak";
            sfd.FileName = DateTime.Now.ToString("yyyyMMddHHmm") + ".bak";
            
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string dbFile = GetDBFilePath();
                File.Copy(dbFile, sfd.FileName, true);
                MessageBox.Show("备份完成!");
            }
        }

        private void btnDataRestore_Click(object sender, EventArgs e)
        {
            if (!ShowQuestion("恢复数据库将对现有数据覆盖,且需要重新验证用户信息,确定要恢复数据吗?", "恢复数据确认"))
            {
                return;
            }
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "数据备份文件|*.bak";
            ofd.CheckFileExists = true;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string dbFile = GetDBFilePath();
                File.Copy(dbFile, DateTime.Now.ToString("yyyyMMddHHmmss") + ".tmpBak", true);
                File.Copy(ofd.FileName, dbFile, true);
                FrmLogin frm = new FrmLogin();
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    _IsRestoreData = true;
                }
                else
                {
                    MessageBox.Show("验证用户信息失败,点击确定系统退出!");
                    Application.Exit();
                }
            }
        }

        private void btnClearConsume_Click(object sender, EventArgs e)
        {
            if (!ShowQuestion("确实要清除账目流水吗?此操作不可恢复", "清除数据确认"))
            {
                return;
            }
            _ALObj.ClearConsume();
            ShowMessage("清除数据完成!");
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            if (!ShowQuestion("确实要清除所有数据吗?此操作不可回复", "清除数据确认"))
            {
                return;
            }
            _ALObj.ClearAllData();
            ShowMessage("清除数据完成!");
        }
    }
}