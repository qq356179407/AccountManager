﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;

namespace AccountManager.ChaXunTongJi
{
    public partial class FrmLeiXingTongJi : BaseForm
    {
        public FrmLeiXingTongJi()
        {
            InitializeComponent();
        }

        ALLeiXingTongJi _ALObj;
        ALLeiXingTongJi.SearchModel _SearchModel;

        void BindDataSource()
        {
            decimal sumAmount;
            dcm1.SetValueToClassObj(_SearchModel);
            var sResult = _ALObj.GetLeiXingTongJiData(_SearchModel, out sumAmount);
            dgv.DataSource = sResult;
            //tslbRecordCount.Text = sResult == null ? "0" : sResult.Count.ToString();
            tslbRecordCount.Text = sResult == null ? "0" : sResult.Rows.Count.ToString();
            tslbSumAmount.Text = sumAmount.ToString();
        }

        private void FrmLeiXingTongJi_Load(object sender, EventArgs e)
        {
            _ALObj = new ALLeiXingTongJi();
            _SearchModel = new ALLeiXingTongJi.SearchModel();
            ComboxListHelper.SetAccount(comboxList1, txtAccount);
            ComboxListHelper.SetConsumeType(comboxList1, txtConsumeType, AL.Enums.EnConsumeTypeCategory.None);
            //dgv.AutoGenerateColumns = true;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dtpEndDate.Value = _ALObj.GetNowTime();
            dtpStartDate.Value = _ALObj.GetNowTime();
            txtAccount.Tag = null;
            txtAccount.Clear();
            txtConsumeType.Tag = null;
            txtConsumeType.Clear();
        }
    }
}
