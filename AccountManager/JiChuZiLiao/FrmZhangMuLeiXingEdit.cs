﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.TableModel.Model;
using AccountManager.AL;
using ZhCun.Framework.Common.Helpers;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmZhangMuLeiXingEdit : BaseForm
    {
        public FrmZhangMuLeiXingEdit(EditFormHandle<TConsumeType> EditHandle)
            : this(EditHandle, null)
        {
        }
        public FrmZhangMuLeiXingEdit(EditFormHandle<TConsumeType> EditHandle, VConsumeType model)
        {
            InitializeComponent();
            this.EditHandle = EditHandle;
            this._Model = model;
        }

        EditFormHandle<TConsumeType> EditHandle;
        VConsumeType _Model;

        private void FrmZhangMuLeiXingEdit_Load(object sender, EventArgs e)
        {
            ComboxHelper.SetConsumeTypeCategory(coBoxCategory);
            if (_Model == null)
            {
                coBoxCategory.SelectedIndex = 0;
            }
            else
            {
                dcm1.SetControlValue(_Model);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            TConsumeType model = new TConsumeType();
            dcm1.SetValueToClassObj(model);
            string errMsg;
            bool r = EditHandle(model, out errMsg);
            if (!r)
            {
                ShowMessage(errMsg);
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtTypeName_Leave(object sender, EventArgs e)
        {
            string py = StringHelper.GetPinYin(txtTypeName.Text, true);
            txtTypeNamePY.Text = py;
        }
    }
}
