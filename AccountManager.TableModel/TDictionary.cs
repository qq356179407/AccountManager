using System;
using System.Collections.Generic;
using System.Text;
using ZhCun.Framework.Common.Models.TableModel;

namespace AccountManager.TableModel.Model
{
	public class TDictionary : ModelBase
	{
		private System.Int32 _DictKey;
		/// <summary>
		/// DictKey
		/// </summary>
		[ModelAttribute(ColumnName = CNDictKey, IsPrimaryKey = true)]
		public System.Int32 DictKey
		{
			get { return _DictKey; }
			set { _DictKey = value; }
		}

		private System.String _DictKeyPrompt;
		/// <summary>
		/// DictKeyPrompt
		/// </summary>
		[ModelAttribute(ColumnName = CNDictKeyPrompt)]
		public System.String DictKeyPrompt
		{
			get { return _DictKeyPrompt; }
			set { _DictKeyPrompt = value; }
		}

		private System.Int32 _DictValue;
		/// <summary>
		/// DictValue
		/// </summary>
		[ModelAttribute(ColumnName = CNDictValue)]
		public System.Int32 DictValue
		{
			get { return _DictValue; }
			set { _DictValue = value; }
		}

		private System.String _DictValuePrompt;
		/// <summary>
		/// DictValuePrompt
		/// </summary>
		[ModelAttribute(ColumnName = CNDictValuePrompt)]
		public System.String DictValuePrompt
		{
			get { return _DictValuePrompt; }
			set { _DictValuePrompt = value; }
		}

		#region 静态的字段名的方法
		public const string CNDictKey = "DictKey";
		public const string CNDictKeyPrompt = "DictKeyPrompt";
		public const string CNDictValue = "DictValue";
		public const string CNDictValuePrompt = "DictValuePrompt";
		#endregion

	}
}
