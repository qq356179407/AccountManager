﻿namespace AccountManager
{
    partial class FrmZhuangZhang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAccountOut = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAccountIn = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.txtAmount = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.txtOutRemark = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.txtConsumeType = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.comboxList1 = new ZhCun.Framework.WinCommon.Components.ComboxList(this.components);
            this.dcm1 = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "转出账户:";
            // 
            // txtAccountOut
            // 
            this.comboxList1.SetColumns(this.txtAccountOut, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtAccountOut, 0);
            this.txtAccountOut.EmptyTextTip = "回车选择转出账户";
            this.txtAccountOut.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtAccountOut, 0);
            this.comboxList1.SetIsUse(this.txtAccountOut, false);
            this.dcm1.SetIsUse(this.txtAccountOut, true);
            this.txtAccountOut.Location = new System.Drawing.Point(107, 28);
            this.txtAccountOut.Name = "txtAccountOut";
            this.comboxList1.SetNextControl(this.txtAccountOut, this.txtAccountIn);
            this.txtAccountOut.Size = new System.Drawing.Size(196, 21);
            this.txtAccountOut.TabIndex = 0;
            this.dcm1.SetTagColumnName(this.txtAccountOut, "OutAccountId");
            this.dcm1.SetTextColumnName(this.txtAccountOut, null);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "转入账户:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "转入金额:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "转入说明:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "账目类型:";
            // 
            // txtAccountIn
            // 
            this.comboxList1.SetColumns(this.txtAccountIn, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtAccountIn, 0);
            this.txtAccountIn.EmptyTextTip = "回车选择转入账户";
            this.txtAccountIn.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtAccountIn, 0);
            this.comboxList1.SetIsUse(this.txtAccountIn, false);
            this.dcm1.SetIsUse(this.txtAccountIn, true);
            this.txtAccountIn.Location = new System.Drawing.Point(107, 59);
            this.txtAccountIn.Name = "txtAccountIn";
            this.comboxList1.SetNextControl(this.txtAccountIn, this.txtConsumeType);
            this.txtAccountIn.Size = new System.Drawing.Size(196, 21);
            this.txtAccountIn.TabIndex = 1;
            this.dcm1.SetTagColumnName(this.txtAccountIn, "InAccountId");
            this.dcm1.SetTextColumnName(this.txtAccountIn, null);
            // 
            // txtAmount
            // 
            this.comboxList1.SetColumns(this.txtAmount, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtAmount, 0);
            this.txtAmount.EmptyTextTip = "输入转入金额";
            this.txtAmount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtAmount, 0);
            this.comboxList1.SetIsUse(this.txtAmount, false);
            this.dcm1.SetIsUse(this.txtAmount, true);
            this.txtAmount.Location = new System.Drawing.Point(107, 121);
            this.txtAmount.Name = "txtAmount";
            this.comboxList1.SetNextControl(this.txtAmount, this.txtOutRemark);
            this.txtAmount.Size = new System.Drawing.Size(196, 21);
            this.txtAmount.TabIndex = 3;
            this.dcm1.SetTagColumnName(this.txtAmount, null);
            this.dcm1.SetTextColumnName(this.txtAmount, "Amount");
            // 
            // txtOutRemark
            // 
            this.comboxList1.SetColumns(this.txtOutRemark, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtOutRemark, 0);
            this.txtOutRemark.EmptyTextTip = null;
            this.txtOutRemark.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtOutRemark, 0);
            this.comboxList1.SetIsUse(this.txtOutRemark, false);
            this.dcm1.SetIsUse(this.txtOutRemark, true);
            this.txtOutRemark.Location = new System.Drawing.Point(107, 152);
            this.txtOutRemark.Name = "txtOutRemark";
            this.comboxList1.SetNextControl(this.txtOutRemark, this.txtOutRemark);
            this.txtOutRemark.Size = new System.Drawing.Size(196, 21);
            this.txtOutRemark.TabIndex = 4;
            this.dcm1.SetTagColumnName(this.txtOutRemark, null);
            this.dcm1.SetTextColumnName(this.txtOutRemark, "OutRemark");
            // 
            // txtConsumeType
            // 
            this.comboxList1.SetColumns(this.txtConsumeType, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtConsumeType, 0);
            this.txtConsumeType.EmptyTextTip = "回车选择账目类型";
            this.txtConsumeType.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtConsumeType, 0);
            this.comboxList1.SetIsUse(this.txtConsumeType, false);
            this.dcm1.SetIsUse(this.txtConsumeType, true);
            this.txtConsumeType.Location = new System.Drawing.Point(107, 90);
            this.txtConsumeType.Name = "txtConsumeType";
            this.comboxList1.SetNextControl(this.txtConsumeType, this.txtAmount);
            this.txtConsumeType.Size = new System.Drawing.Size(196, 21);
            this.txtConsumeType.TabIndex = 2;
            this.dcm1.SetTagColumnName(this.txtConsumeType, "ConsumeTypeId");
            this.dcm1.SetTextColumnName(this.txtConsumeType, null);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(58, 196);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 26);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(228, 196);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 26);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // comboxList1
            // 
            this.comboxList1.GridViewDataSource = null;
            this.comboxList1.MaxRowCount = 5;
            // 
            // FrmZhuangZhang
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(351, 252);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtConsumeType);
            this.Controls.Add(this.txtOutRemark);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.txtAccountIn);
            this.Controls.Add(this.txtAccountOut);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmZhuangZhang";
            this.Text = "账户转帐";
            this.Load += new System.EventHandler(this.FrmZhuangZhang_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.txtAccountOut, 0);
            this.Controls.SetChildIndex(this.txtAccountIn, 0);
            this.Controls.SetChildIndex(this.txtAmount, 0);
            this.Controls.SetChildIndex(this.txtOutRemark, 0);
            this.Controls.SetChildIndex(this.txtConsumeType, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtAccountOut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtAccountIn;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtAmount;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtOutRemark;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtConsumeType;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnClose;
        private ZhCun.Framework.WinCommon.Components.ComboxList comboxList1;
        private ZhCun.Framework.WinCommon.Components.DCM dcm1;
    }
}