﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountManager.Notifys
{
    public class NotifyHelper
    {
        static FrmNormalNotify _FrmNormalNotify;
        /// <summary>
        /// 打开普通通知,内容
        /// </summary>
        /// <param name="content"></param>
        public static void ShowNormalNotify(string content)
        {
            if (_FrmNormalNotify == null || _FrmNormalNotify.IsDisposed)
            {
                _FrmNormalNotify = new FrmNormalNotify();
            }
            _FrmNormalNotify.lbCentent.Text = content;
            _FrmNormalNotify.ShowWindows();
        }

        static FrmUpdateNotify _FrmUpdateNotify;

        public static void ShowUpdateNotify()
        {
            if (_FrmUpdateNotify == null || _FrmUpdateNotify.IsDisposed)
            {
                _FrmUpdateNotify = new FrmUpdateNotify();
            }
            _FrmUpdateNotify.ShowWindows();
        }

    }
}
