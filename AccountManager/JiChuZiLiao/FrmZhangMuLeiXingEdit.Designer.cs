﻿namespace AccountManager.JiChuZiLiao
{
    partial class FrmZhangMuLeiXingEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTypeName = new System.Windows.Forms.TextBox();
            this.txtTypeNamePY = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.dcm1 = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.coBoxCategory = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.chBoxIsPublic = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "类型名称:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "助记码:";
            // 
            // txtTypeName
            // 
            this.dcm1.SetIsUse(this.txtTypeName, true);
            this.txtTypeName.Location = new System.Drawing.Point(91, 17);
            this.txtTypeName.Name = "txtTypeName";
            this.txtTypeName.Size = new System.Drawing.Size(202, 21);
            this.txtTypeName.TabIndex = 0;
            this.dcm1.SetTagColumnName(this.txtTypeName, "Id");
            this.dcm1.SetTextColumnName(this.txtTypeName, "TypeName");
            this.txtTypeName.Leave += new System.EventHandler(this.txtTypeName_Leave);
            // 
            // txtTypeNamePY
            // 
            this.txtTypeNamePY.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.dcm1.SetIsUse(this.txtTypeNamePY, true);
            this.txtTypeNamePY.Location = new System.Drawing.Point(91, 45);
            this.txtTypeNamePY.Name = "txtTypeNamePY";
            this.txtTypeNamePY.Size = new System.Drawing.Size(202, 21);
            this.txtTypeNamePY.TabIndex = 1;
            this.dcm1.SetTagColumnName(this.txtTypeNamePY, null);
            this.dcm1.SetTextColumnName(this.txtTypeNamePY, "TypeNamePY");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "备注:";
            // 
            // txtRemark
            // 
            this.dcm1.SetIsUse(this.txtRemark, true);
            this.txtRemark.Location = new System.Drawing.Point(91, 130);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(202, 21);
            this.txtRemark.TabIndex = 3;
            this.dcm1.SetTagColumnName(this.txtRemark, null);
            this.dcm1.SetTextColumnName(this.txtRemark, "Remark");
            // 
            // coBoxCategory
            // 
            this.coBoxCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coBoxCategory.FormattingEnabled = true;
            this.dcm1.SetIsUse(this.coBoxCategory, true);
            this.coBoxCategory.Items.AddRange(new object[] {
            "支出",
            "收入",
            "对账"});
            this.coBoxCategory.Location = new System.Drawing.Point(91, 73);
            this.coBoxCategory.Name = "coBoxCategory";
            this.coBoxCategory.Size = new System.Drawing.Size(202, 20);
            this.coBoxCategory.TabIndex = 2;
            this.dcm1.SetTagColumnName(this.coBoxCategory, "Category");
            this.dcm1.SetTextColumnName(this.coBoxCategory, "CategoryName");
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(52, 168);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 26);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(233, 168);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 26);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "类型分类:";
            // 
            // chBoxIsPublic
            // 
            this.chBoxIsPublic.AutoSize = true;
            this.dcm1.SetIsUse(this.chBoxIsPublic, true);
            this.chBoxIsPublic.Location = new System.Drawing.Point(91, 104);
            this.chBoxIsPublic.Name = "chBoxIsPublic";
            this.chBoxIsPublic.Size = new System.Drawing.Size(120, 16);
            this.chBoxIsPublic.TabIndex = 7;
            this.dcm1.SetTagColumnName(this.chBoxIsPublic, null);
            this.chBoxIsPublic.Text = "允许其它用户使用";
            this.dcm1.SetTextColumnName(this.chBoxIsPublic, "IsPublic");
            this.chBoxIsPublic.UseVisualStyleBackColor = true;
            // 
            // FrmZhangMuLeiXingEdit
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(358, 219);
            this.Controls.Add(this.chBoxIsPublic);
            this.Controls.Add(this.coBoxCategory);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.txtTypeNamePY);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTypeName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmZhangMuLeiXingEdit";
            this.Text = "账目类型";
            this.Load += new System.EventHandler(this.FrmZhangMuLeiXingEdit_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtTypeName, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtTypeNamePY, 0);
            this.Controls.SetChildIndex(this.txtRemark, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.coBoxCategory, 0);
            this.Controls.SetChildIndex(this.chBoxIsPublic, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTypeName;
        private System.Windows.Forms.TextBox txtTypeNamePY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRemark;
        private ZhCun.Framework.WinCommon.Components.DCM dcm1;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox coBoxCategory;
        private System.Windows.Forms.CheckBox chBoxIsPublic;
    }
}