﻿namespace AccountManager.JiChuZiLiao
{
    partial class FrmZhangHuEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtActName = new System.Windows.Forms.TextBox();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.txtActNamePY = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dcm1 = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(284, 156);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 26);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "退出";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(48, 156);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 26);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtActName
            // 
            this.dcm1.SetIsUse(this.txtActName, true);
            this.txtActName.Location = new System.Drawing.Point(100, 21);
            this.txtActName.Name = "txtActName";
            this.txtActName.Size = new System.Drawing.Size(246, 21);
            this.txtActName.TabIndex = 9;
            this.dcm1.SetTagColumnName(this.txtActName, "Id");
            this.dcm1.SetTextColumnName(this.txtActName, "ActName");
            this.txtActName.Leave += new System.EventHandler(this.txtActName_Leave);
            // 
            // txtRemark
            // 
            this.dcm1.SetIsUse(this.txtRemark, true);
            this.txtRemark.Location = new System.Drawing.Point(100, 81);
            this.txtRemark.Multiline = true;
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(246, 58);
            this.txtRemark.TabIndex = 11;
            this.dcm1.SetTagColumnName(this.txtRemark, null);
            this.dcm1.SetTextColumnName(this.txtRemark, "Remark");
            // 
            // txtActNamePY
            // 
            this.dcm1.SetIsUse(this.txtActNamePY, true);
            this.txtActNamePY.Location = new System.Drawing.Point(100, 52);
            this.txtActNamePY.Name = "txtActNamePY";
            this.txtActNamePY.Size = new System.Drawing.Size(246, 21);
            this.txtActNamePY.TabIndex = 10;
            this.dcm1.SetTagColumnName(this.txtActNamePY, null);
            this.dcm1.SetTextColumnName(this.txtActNamePY, "ActNamePY");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 16;
            this.label3.Text = "备注:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 14;
            this.label2.Text = "账户名称:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "助记码:";
            // 
            // FrmZhangHuEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 205);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtActName);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.txtActNamePY);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmZhangHuEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "账户";
            this.Load += new System.EventHandler(this.FrmZhangHuEdit_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtActNamePY, 0);
            this.Controls.SetChildIndex(this.txtRemark, 0);
            this.Controls.SetChildIndex(this.txtActName, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox txtActName;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.TextBox txtActNamePY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ZhCun.Framework.WinCommon.Components.DCM dcm1;
    }
}