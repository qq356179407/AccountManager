﻿namespace AccountManager.JiChuZiLiao
{
    partial class FrmZhangMuLeiXing_old
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmZhangMuLeiXing));
            this.toolStripTop1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnAddRoot = new System.Windows.Forms.ToolStripButton();
            this.tsBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tsBtnEditor = new System.Windows.Forms.ToolStripButton();
            this.tsBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtnClose = new System.Windows.Forms.ToolStripButton();
            this.tsBtnSelected = new System.Windows.Forms.ToolStripButton();
            this.tv = new System.Windows.Forms.TreeView();
            this.treeViewEx1 = new ZhCun.Framework.WinCommon.Components.TreeViewEx(this.components);
            this.toolStripTop1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripTop1
            // 
            this.toolStripTop1.AutoSize = false;
            this.toolStripTop1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnAddRoot,
            this.tsBtnAdd,
            this.tsBtnEditor,
            this.tsBtnDelete,
            this.toolStripSeparator5,
            this.tsBtnClose,
            this.tsBtnSelected});
            this.toolStripTop1.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop1.Name = "toolStripTop1";
            this.toolStripTop1.Size = new System.Drawing.Size(423, 29);
            this.toolStripTop1.TabIndex = 18;
            this.toolStripTop1.Text = "toolStrip2";
            // 
            // tsBtnAddRoot
            // 
            this.tsBtnAddRoot.BackColor = System.Drawing.SystemColors.Control;
            this.tsBtnAddRoot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnAddRoot.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnAddRoot.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnAddRoot.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnAddRoot.Image")));
            this.tsBtnAddRoot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnAddRoot.Name = "tsBtnAddRoot";
            this.tsBtnAddRoot.Size = new System.Drawing.Size(72, 26);
            this.tsBtnAddRoot.Text = "增加根节点";
            this.tsBtnAddRoot.Click += new System.EventHandler(this.tsBtnAddRoot_Click);
            // 
            // tsBtnAdd
            // 
            this.tsBtnAdd.BackColor = System.Drawing.SystemColors.Control;
            this.tsBtnAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnAdd.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnAdd.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnAdd.Image")));
            this.tsBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnAdd.Name = "tsBtnAdd";
            this.tsBtnAdd.Size = new System.Drawing.Size(72, 26);
            this.tsBtnAdd.Text = "增加子节点";
            this.tsBtnAdd.Click += new System.EventHandler(this.tsBtnAdd_Click);
            // 
            // tsBtnEditor
            // 
            this.tsBtnEditor.BackColor = System.Drawing.SystemColors.Control;
            this.tsBtnEditor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnEditor.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnEditor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnEditor.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnEditor.Image")));
            this.tsBtnEditor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnEditor.Name = "tsBtnEditor";
            this.tsBtnEditor.Size = new System.Drawing.Size(36, 26);
            this.tsBtnEditor.Text = "编辑";
            this.tsBtnEditor.Click += new System.EventHandler(this.tsBtnEditor_Click);
            // 
            // tsBtnDelete
            // 
            this.tsBtnDelete.BackColor = System.Drawing.SystemColors.Control;
            this.tsBtnDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnDelete.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnDelete.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnDelete.Image")));
            this.tsBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnDelete.Name = "tsBtnDelete";
            this.tsBtnDelete.Size = new System.Drawing.Size(36, 26);
            this.tsBtnDelete.Text = "删除";
            this.tsBtnDelete.Click += new System.EventHandler(this.tsBtnDelete_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 29);
            // 
            // tsBtnClose
            // 
            this.tsBtnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnClose.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnClose.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnClose.Margin = new System.Windows.Forms.Padding(0, 1, 15, 2);
            this.tsBtnClose.Name = "tsBtnClose";
            this.tsBtnClose.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.tsBtnClose.Size = new System.Drawing.Size(51, 26);
            this.tsBtnClose.Text = "退出";
            this.tsBtnClose.Click += new System.EventHandler(this.tsBtnClose_Click);
            // 
            // tsBtnSelected
            // 
            this.tsBtnSelected.BackColor = System.Drawing.SystemColors.Control;
            this.tsBtnSelected.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnSelected.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnSelected.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnSelected.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnSelected.Image")));
            this.tsBtnSelected.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnSelected.Name = "tsBtnSelected";
            this.tsBtnSelected.Size = new System.Drawing.Size(60, 26);
            this.tsBtnSelected.Text = "选中退出";
            this.tsBtnSelected.Click += new System.EventHandler(this.tsBtnSelected_Click);
            // 
            // tv
            // 
            this.tv.AllowDrop = true;
            this.tv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tv.FullRowSelect = true;
            this.tv.Location = new System.Drawing.Point(0, 29);
            this.tv.Name = "tv";
            this.tv.Size = new System.Drawing.Size(423, 492);
            this.tv.TabIndex = 19;
            this.tv.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tv_ItemDrag);
            this.tv.DragDrop += new System.Windows.Forms.DragEventHandler(this.tv_DragDrop);
            this.tv.DragEnter += new System.Windows.Forms.DragEventHandler(this.tv_DragEnter);
            this.tv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tv_KeyDown);
            // 
            // FrmZhangMuLeiXing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 521);
            this.Controls.Add(this.tv);
            this.Controls.Add(this.toolStripTop1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmZhangMuLeiXing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "账目类型";
            this.Load += new System.EventHandler(this.FrmZhangMuLeiXing_Load);
            this.Controls.SetChildIndex(this.toolStripTop1, 0);
            this.Controls.SetChildIndex(this.tv, 0);
            this.toolStripTop1.ResumeLayout(false);
            this.toolStripTop1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStrip toolStripTop1;
        public System.Windows.Forms.ToolStripButton tsBtnAdd;
        public System.Windows.Forms.ToolStripButton tsBtnEditor;
        public System.Windows.Forms.ToolStripButton tsBtnDelete;
        public System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        public System.Windows.Forms.ToolStripButton tsBtnClose;
        private System.Windows.Forms.TreeView tv;
        public System.Windows.Forms.ToolStripButton tsBtnSelected;
        public System.Windows.Forms.ToolStripButton tsBtnAddRoot;
        private ZhCun.Framework.WinCommon.Components.TreeViewEx treeViewEx1;
    }
}